#include "HitObject.h"
#include <stdlib.h>

int HitObject::key[10] = {36,109,182,256,329,402,475};

HitObject::HitObject(char* in)
{
	len = strlen(in);
	origin = new char[len];
	strcpy(origin,in);
	sscanf(origin,"%d,%*d,%d",&position,&time);
}


int HitObject::getPosition()
{
	return position;
}

void HitObject::setPosition(int pos)
{
	char* buf = new char[len+1];
	position = key[pos];
	itoa(position,buf,10);
	int o = 0;
	while(origin[o]!=',')o++;
	if(origin[o+1] == '0')
	{
		while(origin[o++]!=':');
		char buff[256];
		itoa(time,buff,10);

		strcat(buf,",192,");	
		strcat(buf,buff);
		strcat(buf,",1,0,");
	}
	strcat(buf,origin+o);
	

	//delete origin;
	origin = buf;
}


int HitObject::getTime()
{
	return time;
}

void HitObject::putsOrigin()
{
	puts(origin);
}


HitObject::~HitObject()
{
	delete origin;
}