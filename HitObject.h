#include <stdio.h>
#include <string.h>
#define KEY 7
class HitObject
{
private:
	static int key[10];
	char* origin;
	int len;
	int position;
	int time;

	
public:
	bool operator< (const HitObject & r);
	HitObject(char* in);
	int getPosition();
	void setPosition(int pos);
	int getTime();
	void putsOrigin();
	~HitObject();
};


class compare
{
public:
	bool operator()(HitObject* l, HitObject* r)
	{
		return l->getTime() < r->getTime();
	}
};