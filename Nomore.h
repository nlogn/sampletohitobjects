#include <vector>
#include <algorithm>
using namespace std;
#include "HitObject.h"



class Nomore
{
private:
	vector<HitObject*> objs;
	void sort_objs();

public:
	Nomore(char* in, char* out);
	~Nomore();
	void NomoreSample();
	void NomoreOverlappedNote();
	void OutResult();
	HitObject* SampleToHitObject(char* BUF,bool strybd);
};