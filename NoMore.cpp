#include "Nomore.h"

Nomore::Nomore(char* in, char* out)
{
	freopen(in,"r",stdin);
	freopen(out,"w",stdout);
}

HitObject* Nomore::SampleToHitObject(char* BUF, bool strybd)
{
	char sampleChecker[32];
	char filename[32];

	char rename[32];
	int r = 0;

	int time,dummy,volume;
	if(strybd)
	{
		sscanf(BUF,"%d,%d,%*d,%[^ ','],%d,",&dummy,&time,filename,&volume);
		if(dummy != 5) return NULL;
	}
	else
	{
		sscanf(BUF,"%[^ ','],%d,%*d,%[^ ','],%d",sampleChecker,&time,filename,&volume);
		if( strcmp(sampleChecker,"Sample") != 0 ) return NULL;
	}
	char OUT[1024];
	for(int i=0;i<strlen(filename);i++)
	{
		if(filename[i]=='"')continue;
		rename[r++] = filename[i];
	}
	rename[r] = NULL;
	sprintf(OUT,"%d,%d,%d,%d,%d,0:0:0:%d:%s",256,192,time,1,0,volume,rename);
	return new HitObject(OUT);
}

void Nomore::NomoreSample()
{
	bool E,EE,T,H;
	EE=E=T=H = false;
	char BUF[1024];

	while( gets(BUF) != NULL )
	{
		if( strcmp(BUF,"[Events]") == 0 )
		{
			E = true;
			continue;
		}
		if( strcmp(BUF,"//Storyboard Sound Samples") == 0 )
		{
			EE = true;
			continue;
		}


		if( strcmp(BUF, "[TimingPoints]") == 0 )
		{
			T = true;
			goto PUTS;
		}


		if( strcmp(BUF, "[HitObjects]") == 0)
		{
			H = true;
			goto PUTS;
		}



		if(H)
		{
			objs.push_back(new HitObject(BUF));
			continue;
		}

		if(T)
		{
			goto PUTS;
		}

		if(E)
		{
			
			HitObject* obj;
			obj = SampleToHitObject(BUF,EE);
			if(obj!=NULL) objs.push_back(obj);
			continue;
		}


		PUTS:
		puts(BUF);
	}
}

void Nomore::NomoreOverlappedNote()
{
	int k,kpos,MIN;
	int now_time = -1;
	int K_BIT = 0;

	int latest[KEY];
	for(k=0;k<KEY;k++) latest[k] = 0;
	sort(objs.begin(),objs.end(),compare());
	vector<HitObject*>::iterator it;
	for(it = objs.begin(); it != objs.end(); ++it)
	{
		if( now_time != (*it)->getTime() )
		{
			now_time = (*it)->getTime();
			K_BIT = 0;
		}

		MIN = 0x7fffffff;
		for(kpos=-1,k=0;k<KEY;k++)
		{
			if( (K_BIT&(1<<k)) == 0 &&  latest[k] < MIN )
			{
				MIN = latest[k];
				kpos = k;
			}
		}
		if(kpos==-1) // ���
			kpos = 3;
		K_BIT |= (1<<kpos);
		latest[kpos] = now_time;
		
		(*it)->setPosition(kpos);
	}

}

void Nomore::OutResult()
{
	vector<HitObject*>::iterator it;
	for(it = objs.begin(); it != objs.end(); ++it) (*it)->putsOrigin();
}

Nomore::~Nomore()
{
	
}